package main

import "fmt"

var a int = 42
var b string = "James  Bond 1"
var c bool = true

type mynumber int

var X mynumber

func main() {

	x := 42
	y := "James Bond"
	z := true
	fmt.Println(X)
	fmt.Printf("%T\n", X)
	X = 143
	fmt.Println("value  of  new X", X)
	fmt.Println(x, " ", y, " ", z)
	//fmt.Println(a, b, c)
	s := fmt.Sprintf("%v\t%v\t%v", a, b, c)
	fmt.Println(s)
	//fmt.Sprintf(string(a), "is a", y, "and he ", z, " man")
	/*fmt.Println("Hello Arjun", x)
	fmt.Println("This is to test git", y)
	if !z {
		fmt.Println("final test to check git commity from vs lab", z, "ARJUN BISWAS")
	}
	fmt.Println("final test to check git to VS code sync")
	fmt.Println("check again if History is mainatined") */
}
